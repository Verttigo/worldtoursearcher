const data = require("./Data/airports.json");
const Utils = require("./Utils");
const fs = require("fs");
const Graph = require("graph-data-structure");

let routes = []
let airports = []

const countryList = new Set();

let graph = Graph();

let h = Infinity
let v = 0;

let hopsFinal = []
let visitedFinal = new Set();


function execute(airport, visited = new Set(), hops = []) {
    if (visited.size === 0 && hops.length === 0) {
        hops.push(airport)
        visited.add(Utils.getAirportFromIATA(airport).Country)
        console.log("Start")
    }

    const destinations = graph.adjacent(airport);

    for (const destination of destinations) {
        let country = Utils.getAirportFromIATA(destination).Country;


        if (h > hops.length || visited.size > v) {
            h = hops.length
            v = visited.size
            hopsFinal = hops
            visitedFinal = visited
        }

        if (!visited.has(country)) {
            let dupliHops = hops;
            let dupliVisited = visited;
            dupliVisited.add(country);
            dupliHops.push(destination)
            execute(destination, dupliVisited, dupliHops)
            continue;
        }
    }
}

function lastCountry() {
    console.log("Starting cal")

    const countryNotV = Utils.getCountryNotVisited(visitedFinal)
    const vMap = new Map()

    let index = 0;
    for (const country of countryNotV) {
        index++;
        const bestAirport = Utils.getAirportWMDestinations(country)
        console.log("Starting : " + index + "/" + countryNotV.size)
        for (const hop of hopsFinal) {
            const shortest = graph.shortestPath(hop, bestAirport)
            if (shortest.length === 3) {
                console.log("ShortCut")
                vMap.set(country, shortest)
                break;
            } else if (vMap.has(country) && vMap.get(country).length > shortest.length) {
                vMap.set(country, shortest)
            } else {
                vMap.set(country, shortest)
            }
        }
    }
    for (const entry of vMap.keys()) {
        let dataEntry = vMap.get(entry);
        let indexWTP = hopsFinal.indexOf(dataEntry[0], 0);
        dataEntry.forEach((item, ind) => {
            if (ind !== 0) hopsFinal.splice(indexWTP + ind, 0, item)
        })
        let indexAP = hopsFinal.indexOf(dataEntry[dataEntry.length - 1], 0);
        dataEntry.reverse();
        dataEntry.forEach((item, ind) => {
            if (ind !== 0) hopsFinal.splice(indexAP + ind, 0, item)
        })
    }

    fs.writeFileSync(__dirname + "/Solutions/test.json", JSON.stringify(hopsFinal));
    console.log("Finit")
}


function start() {
    for (const airport of data) {
        airport.destinations.forEach((dest) => {
            let originIATA = Utils.getAirportIATAFromID(airport.AirportID) ?? ""
            let destIATA = Utils.getAirportIATAFromID(dest) ?? ""
            if (originIATA === "" || destIATA === "") return;
            let v1 = [originIATA, destIATA]
            let v2 = [destIATA, originIATA]
            if (!(routes.includes(v1) || routes.includes(v2))) routes.push(v1);
            if (!airports.includes(originIATA)) airports.push(originIATA);
            if (!airports.includes(destIATA)) airports.push(destIATA);
        })
        if (!countryList.has(airport.Country)) countryList.add(airport.Country)
    }
    airports.forEach(graph.addNode)
    routes.forEach(route => graph.addEdge(...route))


    execute('GVA')
    console.log("Finished first phase")
    lastCountry()


}

start()
