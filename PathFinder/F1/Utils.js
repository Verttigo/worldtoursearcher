const data = require("./Data/airports.json");

exports.getCoordinatesFromID = (id) => {
    for (const entry of data) {
        if (entry.AirportID === id) {
            return [entry.Latitude, entry.Longitude]
        }
    }
}
exports.getCoordinatesFromIATA = (IATA) => {
    for (const entry of data) {
        if (entry.IATA === IATA) {
            return [entry.Latitude, entry.Longitude]
        }
    }
}

exports.alreadyWent = (airport, hops) => {
    for (const hop of hops) {
        if (hops.has(airport)) return true;
    }
    return false;
}


exports.calCrow = (latOrigin, lonOrigin, latDest, LongDest) => {
    let R = 6371; // km
    let dLat = this.toRad(latDest - latOrigin);
    let dLon = this.toRad(LongDest - lonOrigin);
    let lat1 = this.toRad(latOrigin);
    let lat2 = this.toRad(latDest);

    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;
    return d;
}

function getWeightFromKM(km) {
    switch (km) {
        case 700 >= km:
            return 0;
        case 1400 >= km && km > 700:
            return 1;
        case 2100 >= km && km > 1400:
            return 2;
        case km > 2100:
            return 3;
    }
}

exports.havePlacesToGo = (destination, visited) => {
    const destinations = this.getAirportFromIATA(destination).destinations;
    for (const dest of destinations) {
        const countryDest = this.getAirportFromID(dest)
        if (!visited.has(countryDest)) return true;
    }
    return false;
}

exports.getLastValue = (set) => {
    let value;
    for (value of set) ;
    return value;
}

function getALlCountry() {
    let countryList = new Set()
    for (const airport of data) {
        if (!countryList.has(airport.Country)) countryList.add(airport.Country)
    }
    return countryList;
}


exports.calWeight = (from, to, hops) => {
    let weight = 0;
    let coordFrom = this.getCoordinatesFromIATA(from)
    let coordTo = this.getCoordinatesFromIATA(to)
    let distance = getWeightFromKM(this.calCrow(coordFrom[0], coordFrom[1], coordTo[0], coordTo[1]));
    let aW = this.alreadyWent(to, hops)

    if (from.Country === to.Country) weight += 1;
    if (aW) weight += 2;
    weight += distance;
    return Number(weight.toFixed(0));
}

exports.bestDestination = (from, destinations, hops) => {
    let currentBest = "";
    let currentWeight = Infinity;
    for (const dest of destinations) {
        let weight = this.calWeight(from, dest, hops)
        if (currentWeight > weight) {
            currentWeight = weight;
            currentBest = dest;
        }
    }
    return [currentBest, currentWeight]
}

exports.toRad = (Value) => {
    return Value * Math.PI / 180;
}

exports.getAirportIATAFromID = (id) => {
    for (const airport of data) {
        if (airport.AirportID === id) {
            return airport.IATA;
        }
    }
}

exports.getAirportFromID = (id) => {
    for (const airport of data) {
        if (airport.AirportID === id) {
            return airport;
        }
    }
}


exports.getAirportFromIATA = (IATA) => {
    for (const airport of data) {
        if (airport.IATA === IATA) {
            return airport;
        }
    }
}

exports.getCountryNotVisited = (visited) => {
    let countryList = getALlCountry();
    for (const country of visited) {
        if (countryList.has(country)) countryList.delete(country);
    }
    return countryList
}

exports.getAirportWMDestinations = (country) => {
    let currentAirport = "";
    let destinations = 0;
    for (const airport of data) {
        if (airport.Country === country) {
            if(airport.destinations.length > destinations) {
                currentAirport = airport.IATA;
                destinations = airport.destinations.length;
            }
        }
    }
    return currentAirport;
}

exports.minDistance = (nodes, dist, set) => {
    let minDis = Number.MAX_SAFE_INTEGER;
    let minIndex = -1;

    nodes.forEach((node) => {
        if (
            set[node.value] === false &&
            dist[node.value] <= minDis
        ) {
            minDis = dist[node.value];
            minIndex = node;
        }
    });
    return minIndex;
}
