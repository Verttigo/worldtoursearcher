let map;

let marker = {
    "type": "geojson",
    "data": {
        "type": "FeatureCollection",
        "features": []
    }
}
let lineFlip = {
    "type": "geojson",
    "data": {
        "type": "FeatureCollection",
        "features": []
    }
}
let line = {
    "type": "geojson",
    "data": {
        "type": "FeatureCollection",
        "features": []
    }
}

let last = {}
let flip = true;


function load() {
    mapboxgl.accessToken = 'pk.eyJ1IjoidmVydHRpZ28iLCJhIjoiY2tsZ3dsZzd4MWdqMzJwcDd1bGJ0enZpMiJ9.USwLr5MCs54tEZ2c-ZzU4g';
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/light-v10?optimize=true',
        center: [dataExported[0].longitude, dataExported[0].latitude],
        zoom: 5
    });

    map.on('load', function () {
        // Add an image to use as a custom marker
        map.loadImage(
            'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
            function (error, image) {
                if (error) throw error;
                map.addImage('custom-marker', image);
                dataExported.forEach((item, index) => {
                    createMarker(item.latitude, item.longitude, item.name)
                    createLine(item.latitude, item.longitude, item.name, last)
                    last = item;
                })

                map.addSource("World", marker);
                map.addSource("route", line);
                map.addSource("routeFlip", lineFlip);

                map.addLayer({
                        "id": "Text",
                        "type": "symbol",
                        "source": "World",
                        "layout": {
                            "icon-image": "custom-marker",
                            // get the title name from the source"s "title" property
                            "text-field": ["get", "title"],
                            "text-font": [
                                "Open Sans Semibold",
                                "Arial Unicode MS Bold"
                            ],
                            "text-offset": [0, 1.25],
                            "text-anchor": "top"
                        }
                    }
                );

                map.addLayer({
                    "id": "line",
                    "type": "line",
                    "source": "route",
                    "layout": {
                        "line-join": "round",
                        "line-cap": "round"
                    },
                    "paint": {
                        "line-color": "#888",
                        "line-width": 2
                    }
                });
                map.addLayer({
                    "id": "Direction",
                    "type": "symbol",
                    "source": "route",
                    "layout": {
                        "symbol-placement": "line",
                        "text-font": ["Open Sans Regular"],
                        "text-field": ">",
                        "text-size": 48
                    },
                    "paint": {}
                });

                map.addLayer({
                    "id": "lineFlip",
                    "type": "line",
                    "source": "routeFlip",
                    "layout": {
                        "line-join": "round",
                        "line-cap": "round"
                    },
                    "paint": {
                        "line-color": "#888",
                        "line-width": 2
                    }
                });
                map.addLayer({
                    "id": "DirectionFlip",
                    "type": "symbol",
                    "source": "routeFlip",
                    "layout": {
                        "symbol-placement": "line",
                        "text-font": ["Open Sans Regular"],
                        "text-field": "<",
                        "text-size": 48
                    },
                    "paint": {}
                });
            }
        );
    });
}


function createMarker(latitude, longitude, name) {
    marker.data.features.push({
        // feature for Mapbox DC
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [longitude, latitude]
        },
        "properties": {
            "title": name
        }
    });


}

function createLine(latitude, longitude, name, last) {
    if (last.name !== undefined) {
        if (flip) {
            lineFlip.data.features.push({
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": [
                        [longitude, latitude],
                        [last.longitude, last.latitude],
                    ]
                }
            })
            flip = false;
        } else {
            line.data.features.push({
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": [
                        [longitude, latitude],
                        [last.longitude, last.latitude],
                    ]
                }
            })
            flip = true;
        }
    }
}

