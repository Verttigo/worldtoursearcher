const data = require("./Data/airports.json");
const Utils = require("./Utils");
const fs = require("fs");
const Graph = require("graph-data-structure");

let routes = []
let airports = []

const countryList = new Set();

let graph = Graph();

let h = Infinity
let v = 0;


function execute(airport, visited = new Set(), hops = []) {
    if (visited.size === 0 && hops.length === 0) {
        hops.push(airport)
        visited.add(Utils.getAirportFromIATA(airport).Country)
        console.log("Start")
    }

    const destinations = graph.adjacent(airport);

    for (const destination of destinations) {
        let country = Utils.getAirportFromIATA(destination).Country;

        if (visited.size === countryList.size) {
            if (h > hops.length || visited.size > v) {
                h = hops.length
                v = visited.size
                console.log("--------")
                console.log("Hops : " + h)
                fs.writeFileSync(__dirname + "/Solutions/test5.json", JSON.stringify(hops));
            }
        }

        if (!visited.has(country)) {
            let dupliHops = hops;
            let dupliVisited = visited;

            dupliVisited.add(country);
            dupliHops.push(destination)
            execute(destination, dupliVisited, dupliHops)
            continue;
        } else {
            if (visited.size >= 214) {
                for (const countryNV of Utils.getCountryNotVisited(visited)) {
                    let dupliHops = hops;
                    let dupliVisited = visited;
                    let destFT = Utils.getAirportWMDestinations(countryNV)
                    let shortest = graph.shortestPath(airport, destFT)
                    shortest.shift();

                    for (const travel of shortest) {
                        let countryFT = Utils.getAirportFromIATA(travel).Country;
                        dupliVisited.add(countryFT);
                        dupliHops.push(travel)
                    }
                    execute(destFT, dupliVisited, dupliHops)
                    continue;
                }
            }

        }
    }
}

function lastCountry() {

}


function start() {
    for (const airport of data) {
        airport.destinations.forEach((dest) => {
            let originIATA = Utils.getAirportIATAFromID(airport.AirportID) ?? ""
            let destIATA = Utils.getAirportIATAFromID(dest) ?? ""
            if (originIATA === "" || destIATA === "") return;
            let v1 = [originIATA, destIATA]
            let v2 = [destIATA, originIATA]
            if (!(routes.includes(v1) || routes.includes(v2))) routes.push(v1);
            if (!airports.includes(originIATA)) airports.push(originIATA);
            if (!airports.includes(destIATA)) airports.push(destIATA);
        })
        if (!countryList.has(airport.Country)) countryList.add(airport.Country)
    }
    airports.forEach(graph.addNode)
    routes.forEach(route => graph.addEdge(...route))

    execute('GVA')

}

start()
