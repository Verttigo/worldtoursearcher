const data = require("../Dataset/airports.json");
const Utils = require("./Utils");
const fs = require("fs");
const Graph = require("graph-data-structure");
const {getCountryAirports, placeItem, getCountryInContact, getAirportFromContactList} = require("./Utils");

let routes = []
let airports = []

let graph = Graph();

let v = 0;

let visitedPhaseOne = new Set();
let airportsPhaseOne = new Set();


function execute(airport, visited = new Set(), airportsV = new Set()) {
    if (visited.size === 0) {
        visited.add(Utils.getAirportFromIATA(airport).Country)
        airportsV.add(airport);
        console.log("Start")
    }

    const destinations = graph.adjacent(airport);

    for (const destination of destinations) {
        let country = Utils.getAirportFromIATA(destination).Country;

        if (visited.size > v) {
            v = visited.size
            visitedPhaseOne = visited
            airportsPhaseOne = airportsV
            console.log("-------")
            console.log("Visited : " + visited.size)
            console.log("Airport Visited : " + airportsV.size)
        }

        if (!visited.has(country)) {
            visited.add(country);
            airportsV.add(destination);
            execute(destination, visited, airportsV)
            continue;
        }
    }
}

function lastCountry() {
    console.log("Starting phase 2")

    for (const country of Utils.getCountryNotVisited(visitedPhaseOne)) {
        console.log("Starting pathfinding to " + country)
        let path = []
        let name = null
        for (const airport of getCountryAirports(country)) {
            console.log("Airport " + airport + " of " + country + " getting path...")
            let contactCountries = getCountryInContact(visitedPhaseOne, country)
            for (const contactCountry of contactCountries) {
                let airportV = getAirportFromContactList(contactCountry, airportsPhaseOne);
                if (airportV === undefined) continue;
                let tempPath = graph.shortestPath(airportV, airport);
                tempPath.shift()
                if (path.length === 0 || path.length > tempPath) {
                    path = tempPath
                    name = airportV
                }
            }
        }
        if (path.length > 0) {
            console.log("We find a way for " + country + " pushing into the array...")
            airportsPhaseOne = placeItem(airportsPhaseOne, name, path)
            visitedPhaseOne.add(country);
            console.log("Done for " + country)
        } else {
            console.log("Didn't find a valid path for " + country)
        }
    }

}


function start() {
    for (const airport of data) {
        airport.destinations.forEach((dest) => {
            let originIATA = Utils.getAirportIATAFromID(airport.AirportID) ?? ""
            let destIATA = Utils.getAirportIATAFromID(dest) ?? ""
            if (originIATA === "" || destIATA === "") return;
            let v1 = [originIATA, destIATA]
            let v2 = [destIATA, originIATA]
            if (!(routes.includes(v1) || routes.includes(v2))) routes.push(v1);
            if (!airports.includes(originIATA)) airports.push(originIATA);
            if (!airports.includes(destIATA)) airports.push(destIATA);
        })
    }
    airports.forEach(graph.addNode)
    routes.forEach(route => graph.addEdge(...route))


    execute('GVA')
    console.log("Finished first phase")
    console.log("Country visited : " + visitedPhaseOne.size)
    console.log("Airport visited : " + airportsPhaseOne.size)
    lastCountry()
    console.log("Country visited : " + visitedPhaseOne.size)
    console.log("Airport visited : " + airportsPhaseOne.size)
    fs.writeFileSync("../Solutions/test.json", JSON.stringify([...airportsPhaseOne]));


}

start()
