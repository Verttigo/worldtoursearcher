const data = require("../Dataset/airports.json");
let hops = require("../Solutions/test.json");
const fs = require("fs");

let newFile = [];


function getAirportFromIATA(IATA) {
    for (const airport of data) {
        if (airport.IATA === IATA) {
            return airport;
        }
    }
}

function execute() {
    for (const hop of hops) {
        try {
            let airport = getAirportFromIATA(hop)

            let latitude = airport.Latitude;
            let longitude = airport.Longitude;


            newFile.push({
                latitude,
                longitude,
                name: hop
            })
        } catch (e) {
            console.log(hop)
        }
    }
    fs.writeFileSync(__dirname + "/Web/export.js", JSON.stringify(newFile));
}

execute();

