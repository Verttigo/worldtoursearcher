const data = require("../../Data/airports.json");
let solution = require("../../Data/solution-1.json");
const fs = require("fs");

let newFile = [];

solution.pop();

function getAirportFromIATA(IATA) {
    for (const airport of data) {
        if (airport.IATA === IATA) {
            return airport;
        }
    }
}

function execute() {
    for (const hop of solution) {
        try {
            let airportFrom = getAirportFromIATA(hop.from)
            let airportTo = getAirportFromIATA(hop.to)

            let latitudeFrom = airportFrom.Latitude;
            let longitudeFrom = airportFrom.Longitude;

            let latitudeTo = airportTo.Latitude;
            let longitudeTo = airportTo.Longitude;

            newFile.push({
                latitudeFrom,
                longitudeFrom,
                latitudeTo,
                longitudeTo,
                from: hop.from,
                to: hop.to
            })
        } catch (e) {
            console.log(hop)
        }
    }
    fs.writeFileSync(__dirname + "/Solutions/export.js", JSON.stringify(newFile));
}

execute();

