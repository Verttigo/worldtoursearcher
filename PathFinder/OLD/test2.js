const data = require("../../Data/airports.json"),
    JSONdb = require("simple-json-db"),
    db = new JSONdb("database.json");


function getAirportData(id) {
    for (const entry of data) {
        if (entry.AirportID === id) {
            return entry
        }
    }
}

async function getCoordinates(id) {
    for (const entry of data) {
        if (entry.AirportID === id) {
            return [entry.Latitude, entry.Longitude]
        }
    }
}

function calcDistance(latOrigin, lonOrigin, latDest, LongDest) {
    let R = 6371; // km
    let dLat = toRad(latDest - latOrigin);
    let dLon = toRad(LongDest - lonOrigin);
    let lat1 = toRad(latOrigin);
    let lat2 = toRad(latDest);

    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;
    return d;
}

function toRad(Value) {
    return Value * Math.PI / 180;
}

function getAllCountryName() {
    let country = [];
    for (const countryData of data) {
        if (!country.includes(countryData.Country)) {
            country.push(countryData.Country)
        }
    }
    return country;
}

function getCountryByAirport(id) {
    for (const airportData of data) {
        if (airportData.AirportID === id) {
            return airportData.Country
        }
    }
}

exports.start = async () => {
    for (const entryID of airport.destinations) {
        let data = getAirportData(entryID);
        let originCoord = await getCoordinates(airport.AirportID)
        let destCoord = [data.Latitude, data.Longitude];
        let distance = Number(calcDistance(originCoord[0], originCoord[1], destCoord[0], destCoord[1]).toFixed(0))
        let nt = getAllCountryName().filter((country) => (country !== data.Country && country !== airport.Country));
        db.set(entryID, {
            "km": distance,
            "overlap": 0,
            "startedFrom": airport.AirportID,
            "AlreadyTraveled": [airport.Country, data.Country],
            "NeverTraveled": nt,
            "hops": [
                {
                    "from": airport.AirportID,
                    "to": entryID,
                    "distance": distance
                }
            ]
        })
        //execute(entryID, entryID, data.destinations)
    }
}

function execute(id, AirportID, destinations) {
    let data = db.get(id);
    destinations.forEach((dest) => {
        let dataAirport = getAirportData(dest);
        let originCoord = getCoordinates(AirportID)
        let destCoord = [dataAirport.Latitude, dataAirport.Longitude];
        let distance = Number(calcDistance(originCoord[0], originCoord[1], destCoord[0], destCoord[1]).toFixed(0))
        if (data["AlreadyTraveled"].includes(getCountryByAirport(dest))) {
            data["overlap"] = data["overlap"] + 1;
        } else {
            data["NeverTraveled"] = data["NeverTraveled"].filter((country) => (country !== dataAirport.Country));
            data["AlreadyTraveled"].push(dataAirport.Country)
        }
        data.hops.push({
            "from": AirportID,
            "to": dest,
            "distance": distance
        });
        data["km"] = data["km"] + distance;

        if (data["NeverTraveled"] !== []) {
            execute(id, dest, dataAirport.destinations)
        } else {
            console.log("ID : " + id + " has finished his path")
        }

    })
}

let airport = getAirportData("1665");
let already = getAllCountryName().filter((country) => (country !== airport.Country));
let path = {
    start: "1665",
    possibility: {}
};
pathFinding(airport.Country, airport.AirportID, airport.destinations);

function pathFinding(origin, airportID, destinations) {
    let possibleDest = destinations;
    destinations.forEach((dest) => {
        let dataAirport = getAirportData(dest);
        if (!already.includes(dataAirport.Country) || origin === dataAirport.Country) {
            possibleDest = possibleDest.filter((airportID) => (airportID !== dest))
        }
    })
    path.possibility[airportID] = {

    }
}




