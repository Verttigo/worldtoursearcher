const data = require("../../Data/airports.json");
const fs = require("fs");
const Graph = require("graph-data-structure");


let routes = []
let airports = []
const countryList = new Set();


let graph = Graph();

function getAirportIATA(id) {
    for (const airport of data) {
        if (airport.AirportID === id) {
            return airport.IATA;
        }
    }
}


function getAirportFromIATA(IATA) {
    for (const airport of data) {
        if (airport.IATA === IATA) {
            return airport;
        }
    }
}

function start() {
    for (const airport of data) {
        airport.destinations.forEach((dest) => {
            let originIATA = getAirportIATA(airport.AirportID) ?? ""
            let destIATA = getAirportIATA(dest) ?? ""
            if (originIATA === "" || destIATA === "") return;
            let v1 = [originIATA, destIATA]
            let v2 = [destIATA, originIATA]
            if (!(routes.includes(v1) || routes.includes(v2))) routes.push(v1);
            if (!airports.includes(originIATA)) airports.push(originIATA);
            if (!airports.includes(destIATA)) airports.push(destIATA);
        })
        if (!countryList.has(airport.Country)) {
            countryList.add(airport.Country)
        }
    }


    airports.forEach(graph.addNode)
    routes.forEach(route => graph.addEdge(...route))

    console.log(graph.shortestPath("GVA", "BKK"));


}

let index = 0;

function execute(airport, visited = new Set(), hops = new Map()) {
    //Si n'est pas le premier aéroport met un saut avec les datas
    //if (from !== undefined) hops.set(from, airport);

    //Prend les datas de l'aéroport via IATA
    const currentAirportData = getAirportFromIATA(airport)

    //Regarde les edges par rapport à cette aéroport
    const destinations = graph.adjacent(airport);

    //On ajoute le pays à la liste
    visited.add(airport);


    //On prend tous les edges et on lance le recursif
    for (const destination of destinations) {
        //On prend les data de l'aéroport de destination
        const aiportData = getAirportFromIATA(destination);

        //Si le pays de l'aéroport est déjà inscrit, on y va pas.
        if (!visited.has(aiportData.Country)) {
            //On execute avec la destination
            execute(destination, visited, hops);
        }


        if (destination === 'EUN' && index === 0) {
            console.log(hops);
            index++;
        }

    }


}


start();

