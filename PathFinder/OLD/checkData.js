const data = require("../../Data/airports.json");
let solution = require("../../Data/solution-1.json");

let country = [];
let countryTotal = [];

solution.pop();

function getAirportFromIATA(IATA) {
    for (const airport of data) {
        if (airport.IATA === IATA) {
            return airport;
        }
    }
}

for (const hop of solution) {
    let from = getAirportFromIATA(hop.from)
    let to = getAirportFromIATA(hop.to)

    if (!country.includes(from.Country)) country.push(from.Country);
    if (!country.includes(to.Country)) country.push(to.Country);
}

for (const airport of data) {
    let from = getAirportFromIATA(airport.IATA).Country
    if (!countryTotal.includes(from)) countryTotal.push(from);
}


console.log(country.length)
console.log(countryTotal.length)

solution.forEach((item, index) => {
    if (index !== 0) {
        if (solution[index - 1].to !== solution[index].from) {
            console.log(solution[index - 1].to + " - " + solution[index].from)
        }
    }
})
