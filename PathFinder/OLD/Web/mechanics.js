let map;

let marker = {
    "type": "geojson",
    "data": {
        "type": "FeatureCollection",
        "features": []
    }
}
let line = {
    "type": "geojson",
    "data": {
        "type": "FeatureCollection",
        "features": []
    }
}


function load() {
    mapboxgl.accessToken = 'pk.eyJ1IjoidmVydHRpZ28iLCJhIjoiY2tsZ3dsZzd4MWdqMzJwcDd1bGJ0enZpMiJ9.USwLr5MCs54tEZ2c-ZzU4g';
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/light-v10?optimize=true',
        center: [dataExported[0].longitudeFrom, dataExported[0].latitudeFrom,],
        zoom: 5
    });

    map.on('load', function () {
        // Add an image to use as a custom marker
        map.loadImage(
            'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
            function (error, image) {
                if (error) throw error;
                map.addImage('custom-marker', image);
                dataExported.forEach((item, index) => {
                    createMarker(item.latitudeFrom, item.longitudeFrom, item.latitudeTo, item.longitudeTo, item.from, item.to)
                    createLine(item.latitudeFrom, item.longitudeFrom, item.latitudeTo, item.longitudeTo)
                })

                map.addSource("World", marker);
                map.addSource("route", line);

                map.addLayer({
                        "id": "Text",
                        "type": "symbol",
                        "source": "World",
                        "layout": {
                            "icon-image": "custom-marker",
                            // get the title name from the source"s "title" property
                            "text-field": ["get", "title"],
                            "text-font": [
                                "Open Sans Semibold",
                                "Arial Unicode MS Bold"
                            ],
                            "text-offset": [0, 1.25],
                            "text-anchor": "top"
                        }
                    }
                );

                map.addLayer({
                    "id": "line",
                    "type": "line",
                    "source": "route",
                    "layout": {
                        "line-join": "round",
                        "line-cap": "round"
                    },
                    "paint": {
                        "line-color": "#888",
                        "line-width": 2
                    }
                });
                map.addLayer({
                    "id": "Direction",
                    "type": "symbol",
                    "source": "route",
                    "layout": {
                        "symbol-placement": "line",
                        "text-font": ["Open Sans Regular"],
                        "text-field": ">",
                        "text-size": 48
                    },
                    "paint": {}
                });
            }
        );
    });
}

function createLine(latitudeFrom, longitudeFrom, latitudeTo, longitudeTo) {
    line.data.features.push({
        "type": "Feature",
        "properties": {},
        "geometry": {
            "type": "LineString",
            "coordinates": [
                [longitudeFrom, latitudeFrom],
                [longitudeTo, latitudeTo],
            ]
        }

    });
}


function createMarker(latitudeFrom, longitudeFrom, latitudeTo, longitudeTo, from, to) {
    marker.data.features.push({
            // feature for Mapbox DC
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [longitudeFrom, latitudeFrom]
            },
            "properties": {
                "title": from
            }
        },
        {
            // feature for Mapbox SF
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [longitudeTo, latitudeTo]
            },
            "properties": {
                "title": to
            }
        },
        {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "LineString",
                "coordinates": [
                    [longitudeFrom, latitudeFrom],
                    [longitudeTo, latitudeTo],
                ]
            }
        })
}

