const data = require("../../Data/airports.json");
const fs = require("fs");
const Graph = require("graph-data-structure");

let maxOverLaps = 80;

let routes = []
let airports = []
const countryList = new Set();
let path = 0;
let dateStart = new Date();
let find = 1;
let graph = Graph();

function getAirportIATA(id) {
    for (const airport of data) {
        if (airport.AirportID === id) {
            return airport.IATA;
        }
    }
}

function getCountryFromId(id) {
    for (const entry of data) {
        if (entry.AirportID === id) {
            return entry.Country
        }
    }
}


function getAirportFromIATA(IATA) {
    for (const airport of data) {
        if (airport.IATA === IATA) {
            return airport;
        }
    }
}

function start() {
    for (const airport of data) {
        airport.destinations.forEach((dest) => {
            let originIATA = getAirportIATA(airport.AirportID) ?? ""
            let destIATA = getAirportIATA(dest) ?? ""
            if (originIATA === "" || destIATA === "") return;
            let v1 = [originIATA, destIATA]
            let v2 = [destIATA, originIATA]
            if (!(routes.includes(v1) || routes.includes(v2))) routes.push(v1);
            if (!airports.includes(originIATA)) airports.push(originIATA);
            if (!airports.includes(destIATA)) airports.push(destIATA);
        })
        if (!countryList.has(airport.Country)) {
            countryList.add(airport.Country)
        }
    }


    airports.forEach(graph.addNode)
    routes.forEach(route => graph.addEdge(...route))

    console.log(graph.shortestPath("GVA", "XCH"))

    execute("GVA")

}


function airportContainsNotV(airportID, already) {
    let country = getCountryFromId(airportID);
    if (already.has(country)) {
        return false
    } else {
        return true;
    }
}

function alreadyWent(airport, hops) {
    for(const hop of hops) {
        if(hops.to === airport || hops.from === airport) return true;
    }
    return false;
}


function execute(airport, already = new Set(), overlaps = 0, hops = [], from) {
    if (overlaps > maxOverLaps) {
        airport = null;
        already = null;
        overlaps = null;
        hops = null;
        return;
    }
    if (from !== undefined) hops.push({from: from, to: airport});

    const currentAirportData = getAirportFromIATA(airport)
    const destinations = graph.adjacent(airport);


    already.add(currentAirportData.Country);

    if (already.size > path) {
        let now = new Date();
        let diffMins = Math.round((((now - dateStart) % 86400000) % 3600000) / 60000);
        path++;

        console.log("---------------")
        console.log("OverLaps : " + overlaps)
        console.log("Minute(s) elapsed : " + diffMins)
        console.log("Country processed : " + path);

    }

    if (already.size === 215) {

        console.log("---------------")
        console.log("I found a path, check solutions.json for full resume");
        console.log("Overlaps : " + overlaps)
        console.log("---------------")


        hops.push({
            overlaps,
            time: Math.round((((new Date() - dateStart) % 86400000) % 3600000) / 60000)
        })
        fs.writeFileSync(__dirname + "/Solutions/solution-" + find + ".json", JSON.stringify(hops));
        process.exit()
        return;
    }

    for (const destination of destinations) {
        const aiportData = getAirportFromIATA(destination);


        if (!already.has(aiportData.Country)) {
            if(alreadyWent(destination, hops)) {
                console.log("Merde")
            }

            execute(destination, already, overlaps, hops, airport);
            if (destination === "TGD" && airport === "LJU") {
                console.log(graph.adjacent(airport))
                console.log("From : " + from + " To: " + destination + " Airport : " + airport)
            }

        }


        if (currentAirportData.Country === aiportData.Country && airportContainsNotV(destination, already)) {
            setTimeout(() => {
                execute(destination, already, overlaps++, hops, airport);
            })
            if (destination === "TIV" && airport === "DME") {
                console.log("Second")
            }
            continue;
        }




    }

}


start();

