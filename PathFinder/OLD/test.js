const data = require("../../Data/airports.json")
let country = [];

data.forEach((entry) => {
    if(!(country.includes(entry.Country))) {
        country.push(entry.Country)
    }

})

let airport = getAiportData("1665");
nearestCountry(airport.AirportID, airport.destinations)



function getAiportData(id) {
   for (const entry of data) {
        if(entry.AirportID === id) {
            return entry
        }
    }
}

function nearestCountry(initalID, listDestinations){
    let closest = 99999;
    let initalCoord = getCoordinates("1665");
    for (const entry of listDestinations) {
        let destCoord = getCoordinates(entry)
        let distance = Number(calcCrow(initalCoord[0], initalCoord[1], destCoord[0], destCoord[1]).toFixed(0))
        if(closest > distance) {
            closest = distance;
        }
    }
    console.log(closest)
}

function getCoordinates(id) {
    for (const entry of data) {
        if (entry.AirportID === id) {
            return [entry.Latitude, entry.Longitude]
        }
    }
}

function calcCrow(latOrigin, lonOrigin, latDest, LongDest)
{
    let R = 6371; // km
    let dLat = toRad(latDest-latOrigin);
    let dLon = toRad(LongDest-lonOrigin);
    let lat1 = toRad(latOrigin);
    let lat2 = toRad(latDest);

    let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    let d = R * c;
    return d;
}

function toRad(Value)
{
    return Value * Math.PI / 180;
}


