const data = require("../../Data/airports.json");
const fs = require("fs");
const Graph = require("graph-data-structure");

let routes = []
let airports = []
let finalVisited = new Set();
let finalHops = new Set();
const countryList = new Set();

let graph = Graph();

function getAirportIATAFromID(id) {
    for (const airport of data) {
        if (airport.AirportID === id) {
            return airport.IATA;
        }
    }
}


function getAirportFromIATA(IATA) {
    for (const airport of data) {
        if (airport.IATA === IATA) {
            return airport;
        }
    }
}

function bfs(start) {

    const visited = new Set();

    const hops = new Set();

    const queue = [start]

    hops.add(start)


    while (queue.length > 0) {

        const airport = queue.shift(); // mutates the queue

        const destinations = graph.adjacent(airport);

        for (const destination of destinations) {
            let country = getAirportFromIATA(destination).Country;

            if (visited.size > 210) {
                hops.add(destination)
                finalVisited = visited
                finalHops = hops
                console.log(finalVisited.size)
                let hopsArray = [];
                for (const hop of finalHops) hopsArray.push(hop);
                fs.writeFileSync(__dirname + "/Solutions/test5.json", JSON.stringify(hopsArray));
                process.exit();
            }

            if (!visited.has(country)) {
                visited.add(country);
                queue.push(destination);
                hops.add(destination)
            }

        }
    }

}


function start() {
    for (const airport of data) {
        airport.destinations.forEach((dest) => {
            let originIATA = getAirportIATAFromID(airport.AirportID) ?? ""
            let destIATA = getAirportIATAFromID(dest) ?? ""
            if (originIATA === "" || destIATA === "") return;
            let v1 = [originIATA, destIATA]
            let v2 = [destIATA, originIATA]
            if (!(routes.includes(v1) || routes.includes(v2))) routes.push(v1);
            if (!airports.includes(originIATA)) airports.push(originIATA);
            if (!airports.includes(destIATA)) airports.push(destIATA);
        })
        if (!countryList.has(airport.Country)) {
            countryList.add(airport.Country)
        }
    }

    airports.forEach(graph.addNode)
    routes.forEach(route => graph.addEdge(...route))

    bfs('GVA')
}

start()


process.on('exit', () => {
    console.log(finalVisited.size)
    let hopsArray = [];
    for (const hop of finalHops) hopsArray.push(hop);
    fs.writeFileSync(__dirname + "/Solutions/test5.json", JSON.stringify(hopsArray));
})


