const data = require("../../Data/airports.json");
const fs = require("fs");


function getAirportIATA(id) {
    for (const airport of data) {
        if (airport.AirportID === id) {
            return airport.IATA;
        }
    }
}

function getAirportFromIATA(IATA) {
    for (const airport of data) {
        if (airport.IATA === IATA) {
            return airport;
        }
    }
}


function start() {
    let routes = []
    let airports = []
    let countryList = []
    for (const airport of data) {
        airport.destinations.forEach((dest) => {
            let originIATA = getAirportIATA(airport.AirportID)
            let destIATA = getAirportIATA(dest)
            if (originIATA === "" || destIATA === "") return;
            let v1 = [originIATA, destIATA]
            let v2 = [destIATA, originIATA]
            if (!(routes.includes(v1) || routes.includes(v2))) routes.push(v1);
            if (!airports.includes(originIATA)) airports.push(originIATA);
            if (!airports.includes(destIATA)) airports.push(destIATA);
        })
        if (!countryList.includes(airport.Country)) {
            countryList.push(airport.Country)
        }
    }


// The graph
    const adjacencyList = new Map();

// Add node
    function addNode(airport) {
        adjacencyList.set(airport, []);
    }

// Add edge, undirected
    function addEdge(origin, destination) {
        adjacencyList.get(origin).push(destination);
        adjacencyList.get(destination).push(origin);
    }

// Create the Graph
    airports.forEach(addNode);
    routes.forEach(route => addEdge(...route))

    airports = null;
    routes = null;

    let path = 0;
    let dateStart = new Date();
    let first = true;


    function execute(airport, lastAirport, already = new Set(), overlaps = 0, hops = []) {
        if (overlaps > 5) {
            airport = null;
            already = null;
            overlaps = null;
            hops = null;
            return;
        }

        const queue = [airport]
        while (queue.length > 0) {
            const testing = queue.shift();
            const destinations = adjacencyList.get(testing);

            if(destinations === undefined) return;

            for (const destination of destinations) {
                const currentAirportData = getAirportFromIATA(destination)

                if (first) first = false; else hops.push({from: lastAirport, to: airport});

                if (!already.has(currentAirportData.Country)) {
                    already.add(currentAirportData.Country);
                    if (already.size > path) {
                        let now = new Date();
                        let diffMins = Math.round((((now - dateStart) % 86400000) % 3600000) / 60000);
                        path++;
                        console.log("---------------")
                        console.log("OverLaps : " + overlaps)
                        console.log("Minute(s) elapsed : " + diffMins)
                        console.log(path + "/" + countryList.length)
                    }

                }

                if (already.size === 217) {
                    console.log("---------------")
                    console.log("I found a path, check solutions.json for full resume");
                    console.log("---------------")
                    hops.push({
                        overlaps,
                        time: Math.round((((new Date() - dateStart) % 86400000) % 3600000) / 60000)
                    })
                    fs.writeFileSync(__dirname + "/Solutions/solution-" + "1" + ".json", JSON.stringify(hops));
                    break;
                }

                if (!already.has(currentAirportData.Country)) {
                    already.add(currentAirportData.Country);
                    queue.push(currentAirportData.Country);
                }

            }

        }
    }

    execute("GVA", "GVA")

}


start();

