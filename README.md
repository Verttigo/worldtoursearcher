# World Tour Seacher Algorythm

Basic algorythm to search best way to travel accross all country with minimum "jumps".

A "jump" is a travel between A to B, best case scenario all country are accessible by multiple country and multiple airport.
We don't live in a perject world, take Monaco for example : In the dataset i use, it's only contacting airport is Nice, but Nice is not accessing a lot of internationnal airport, that's why we need to use internal airport, example : "Paris To Nice" -> "Nice to Monaco" and now reverse pathing to join "Paris" again so we can go like to "London".

This algorythm want to find the best possible path through the dataset so all country are visited.
Inside the algorythm we can see part of Dijkstra and DFS method.

At this point, this algorythm is not efficient at all, so that's why after 6 months im working on it again with a french mind and a clear way.

## Getting started
This application is under heavy dev so the files are little bit messy (A lot)
Folders :
    - PathFinder : Main folder with all project file and version
    - F1 -> 3 : Folder with version
    - Web : Web app to see the path of the algorythm
    - Data : Contains dataset
    - Solutions : Where the algorythm put the solution

Just search through the file and don't mind.

More information in the package.json's file.


## Dev Tools

For the dev you will need Node, Git and the NPM modules installed.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
  Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Linux

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following command.

    $ node --version
    v15.6.0

    $ npm --version
    6.14.11

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g

## Install

    $ git clone https://gitlab.com/Verttigo/worldtoursearcher.git
    $ cd worldtoursearcher
    $ npm install

### Packages intallation
After installing node, we will need all the packages that the app need.

      $ cd worldtoursearcher
      $ npm install

---

## Running the project

    $ npm start


## Authors

* **Baptiste Ferrando** - *Initial work* - [Baptiste Ferrando](https://gitlab.com/Verttigo)


